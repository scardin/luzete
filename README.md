# Host

    vps213782.ovh.net
 
# Arranque en background

	# docker-compose up -d

# URLS de la aplicación

	http://vps213782.ovh.net/swagger-ui.html
	
# URLS de componentes de infraestructura de microservicios	

	Netflix OSS Eureka:                     http://vps213782.ovh.net:8761
	
	Netflix OSS Hystrix Dashboard:          http://vps213782.ovh.net:7979
	
	Spring Cloud Configuration Server:


