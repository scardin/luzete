package com.amarokete.energy.pf.service;

import com.amarokete.energy.pf.PriceFetcherApplication;
import com.amarokete.energy.pf.domain.DailyEnergyPrice;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Santiago on 19/10/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PriceFetcherApplication.class})
@WebAppConfiguration
public class EnergyPriceServiceTest {

    @Autowired
    EnergyPriceService priceService;

    @Test
    public void fetchIndicatorPrice() throws IOException {
        priceService.fetchIndicatorPrice(791, DateTime.now());
    }

    @Test
    public void fetchAndSaveEnergyPrice() throws IOException {
        List<Integer> indicators = new ArrayList<Integer>();
        indicators.add(791);
        indicators.add(10229);
        indicators.add(10230);
        indicators.add(10231);

        priceService.fetchAndSaveEnergyPrices(indicators, DateTime.now());
    }

    @Test
    public void getDailyEnergyPrice() throws IOException {
        DailyEnergyPrice ep  = priceService.getIndicatorPrices(791, DateTime.now().minusDays(1));
        System.out.println(ep);
    }

    @Test(expected = IndicatorNotFoundException.class)
    public void getDailyEnergyPriceWrongIndicator() throws IOException {
        priceService.getIndicatorPrices(0,  DateTime.now());
    }
}
