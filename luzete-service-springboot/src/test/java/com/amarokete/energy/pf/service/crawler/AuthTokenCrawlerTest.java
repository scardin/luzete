package com.amarokete.energy.pf.service.crawler;

import com.amarokete.energy.pf.PriceFetcherApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;

/**
 * Created by Santiago on 19/10/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PriceFetcherApplication.class})
@WebAppConfiguration
public class AuthTokenCrawlerTest {

    @Autowired
    AuthTokenCrawler crawler;

    @Test
    public void crawlIndicatorPrice() throws IOException {
        String authToken= crawler.getAuthToken();
    }
}
