package com.amarokete.energy.pf.rest;

import com.amarokete.energy.pf.domain.DailyEnergyPrice;
import com.amarokete.energy.pf.service.EnergyPriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Santiago on 19/10/2015.
 */
@RestController
@RequestMapping(value = "api/energy")
@Api(value = "indicator", description = "Indicator price REST Controller")
public class EnergyPriceResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnergyPriceResource.class);

    private static DateTimeFormatter dateDecoder = DateTimeFormat.forPattern("yyyy-MM-dd");

    @Autowired
    EnergyPriceService service;

    @RequestMapping(value = "/daily/{indicator}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "getPrices", notes = "Returns the daily energy prices for an indicator and a date (yyyy-MM-dd")
    @ApiResponses({
            @ApiResponse(code = 200, message = "The retrieved energy price indicator"),
            @ApiResponse(code = 404, message = "If the indicator does not exists")
    })
    public DailyEnergyPrice getDailyPrices(@PathVariable("indicator") Integer indicator,
                                           @RequestParam(value = "day", required = true) String day) throws IOException {
        LOGGER.debug("Get daily prices for indicator: {} and date {}", indicator, day);

        return service.getIndicatorPrices(indicator, dateDecoder.parseDateTime(day));
    }

    @RequestMapping(value = "/best/{indicator}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "getCheapestHour", notes = "Returns the energy prices for an indicator")
    @ApiResponses({
            @ApiResponse(code = 200, message = "The retrieved energy price indicator"),
            @ApiResponse(code = 404, message = "If the indicator does not exists")
    })
    public Integer getBestHour(@PathVariable("indicator") Integer indicator,
                               @RequestParam(value = "day", required = true) String day,
                               @RequestParam(value = "restriction[]", required = false) String[] restrictions) throws IOException {
        LOGGER.debug("Get best hour for indicator: {}, day = {} with restrictions {}", indicator, day, restrictions);

        DateTime date = dateDecoder.parseDateTime(day);

        List<Interval> timeRestrictions = new ArrayList<>();
        if (restrictions != null) {
            for (String restriction : restrictions) {
                StringTokenizer st = new StringTokenizer(restriction, ",");
                while (st.hasMoreElements()) {
                    String sinterval = st.nextToken();
                    int begin = new Integer(StringUtils.substringBefore(sinterval, "-"));
                    int end = new Integer(StringUtils.substringAfter(sinterval, "-"));

                    timeRestrictions.add(new Interval(date.withHourOfDay(begin), date.withHourOfDay(end)));
                }
            }
        }

        return service.getBestHourPrice(indicator, timeRestrictions, date);
    }
}
