package com.amarokete.energy.pf.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.MimeMappings;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Santiago on 19/10/2015.
 */
@Configuration
public class WebConfigurer implements EmbeddedServletContainerCustomizer {

    private final Logger LOGGER = LoggerFactory.getLogger(WebConfigurer.class);

    /**
     * Set up Mime types.
     */
    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {
        LOGGER.debug("MIME type mapping configuration");
        MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
        mappings.add("html", "text/html;charset=utf-8");
        mappings.add("json", "application/json;charset=utf-8");
        container.setMimeMappings(mappings);
    }
}
