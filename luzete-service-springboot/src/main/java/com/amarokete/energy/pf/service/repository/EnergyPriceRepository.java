package com.amarokete.energy.pf.service.repository;

import com.amarokete.energy.pf.domain.DailyEnergyPrice;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Date;
import java.util.Optional;

/**
 * Created by Santiago on 19/10/2015.
 */
public interface EnergyPriceRepository extends MongoRepository<DailyEnergyPrice, String>, EnergyPriceRepositoryCustom {

}
