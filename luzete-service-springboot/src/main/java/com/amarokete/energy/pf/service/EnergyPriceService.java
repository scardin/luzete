package com.amarokete.energy.pf.service;

import com.amarokete.energy.pf.domain.DailyEnergyPrice;
import com.amarokete.energy.pf.domain.Indicator;
import com.amarokete.energy.pf.service.api.MathInvoker;
import com.amarokete.energy.pf.service.api.RedElectricaInvoker;
import com.amarokete.energy.pf.service.repository.EnergyPriceRepository;
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by Santiago on 19/10/2015.
 */
@Service
public class EnergyPriceService {

    private static Logger LOG = LoggerFactory
            .getLogger(EnergyPriceService.class);

    @Autowired
    EnergyPriceRepository repository;

    @Autowired
    RedElectricaInvoker reeInvoker;

    @Autowired
    MathInvoker mathInvoker;

    public void fetchAndSaveEnergyPrices(List<Integer> indicators, DateTime day) throws IOException {

        DailyEnergyPrice energyPrice = new DailyEnergyPrice();

        for (Integer indicator : indicators) {
            energyPrice.setTimestamp(new Date());
            energyPrice.addIndicator(fetchIndicatorPrice(indicator, day));
        }

        saveEnergyPrice(energyPrice);
    }

    //@HystrixCommand(ignoreExceptions = IndicatorNotFoundException.class)
    public DailyEnergyPrice getIndicatorPrices(Integer indicator, DateTime date) {
        return repository.
                findIndicatorByDay(date.toDate(), indicator).orElseThrow(() -> new IndicatorNotFoundException());
    }

    //@HystrixCommand
    public Indicator fetchIndicatorPrice(Integer indicator, DateTime day) throws IOException {
        return reeInvoker.fetchIndicatorPrice(indicator, day);
    }

    public void saveEnergyPrice(DailyEnergyPrice energyPrice) {
        repository.save(energyPrice);
    }

    //@HystrixCommand(ignoreExceptions = IndicatorNotFoundException.class)
    public Integer getBestHourPrice(Integer indicator, List<Interval> timeRestrictions, DateTime date) {

        DailyEnergyPrice price = this.getIndicatorPrices(indicator, date);

        return mathInvoker.getBestHour(price, timeRestrictions, date.toDate());
    }
}
