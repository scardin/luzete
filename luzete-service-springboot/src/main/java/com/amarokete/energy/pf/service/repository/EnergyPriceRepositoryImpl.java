package com.amarokete.energy.pf.service.repository;

import com.amarokete.energy.pf.domain.DailyEnergyPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Date;
import java.util.Optional;

/**
 * Created by Santiago on 31/10/2015.
 */
public class EnergyPriceRepositoryImpl implements EnergyPriceRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Optional<DailyEnergyPrice> findIndicatorByDay(Date timestamp, Integer indicator) {

        Criteria findDateCriteria = Criteria.where("timestamp").gte(timestamp);
        findDateCriteria.andOperator(Criteria.where("indicators.id").is(indicator));

        Criteria findIndicatorCriteria = Criteria.where("indicators").elemMatch(Criteria.where("id").is(indicator));

        BasicQuery query = new BasicQuery(findDateCriteria.getCriteriaObject(), findIndicatorCriteria.getCriteriaObject());

        return Optional.ofNullable(mongoTemplate.findOne(query, DailyEnergyPrice.class));
    }
}
