package com.amarokete.energy.pf.service.api;

import com.amarokete.energy.pf.domain.Indicator;
import com.amarokete.energy.pf.service.crawler.AuthTokenCrawler;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Santiago on 19/10/2015.
 */
@Service
public class RedElectricaInvoker {

    private static Logger LOG = LoggerFactory
            .getLogger(RedElectricaInvoker.class);

    private static DateTimeFormatter dtf = DateTimeFormat.forPattern("YYYY-MM-DD");

    @Value("${api.endpoint}")
    String url;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    AuthTokenCrawler crawler;

    public Indicator fetchIndicatorPrice(Integer indicatorName, DateTime day) throws IOException {

        String today = day.toString(dtf);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Token token=\"" + getAuthToken() + "\"");
        headers.add("Origin", "https://www.esios.ree.es");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> httpEntity = new HttpEntity<>("", headers);

        Map<String, String> params = new HashMap<>();
        params.put("date", today);
        params.put("indicator", String.valueOf(indicatorName));

        ResponseEntity<String> response = restTemplate.
                exchange(url, HttpMethod.GET, httpEntity, String.class, params);

        return getIndicator(response);
    }

    private Indicator getIndicator(ResponseEntity<String> response) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        mapper.registerModule(new JodaModule());

        ReeResponse ree = mapper.readValue(response.getBody(), ReeResponse.class);

        LOG.info(ree.toString());

        return ree.getIndicator();
    }

    private String getAuthToken() throws IOException {
        return crawler.getAuthToken();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class ReeResponse {

        private Indicator indicator;

        public ReeResponse() {
        }

        public Indicator getIndicator() {
            return indicator;
        }

        public void setIndicator(Indicator indicator) {
            this.indicator = indicator;
        }

        @Override
        public String toString() {
            return "ReeResponse{" +
                    "indicator=" + indicator +
                    '}';
        }
    }
}
