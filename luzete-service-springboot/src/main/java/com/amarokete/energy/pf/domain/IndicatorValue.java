package com.amarokete.energy.pf.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.joda.time.DateTime;

/**
 * Created by Santiago on 19/10/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndicatorValue {

    private Double value;

    private DateTime datetime;

    public IndicatorValue() {

    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public DateTime getDatetime() {
        return datetime;
    }

    public void setDatetime(DateTime datetime) {
        this.datetime = datetime;
    }

    @Override
    public String toString() {
        return "IndicatorValue{" +
                "value=" + value +
                ", datetime=" + datetime +
                '}';
    }
}
