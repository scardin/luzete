package com.amarokete.energy.pf.service.crawler;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by Santiago on 19/10/2015.
 */
@Service
public class AuthTokenCrawler {

    private static Logger LOG = LoggerFactory
            .getLogger(AuthTokenCrawler.class);

    @Value("${crawler.url}")
    String url;

    @Value("${crawler.timeout}")
    Integer timeout;

    @Value("${crawler.userAgent}")
    String userAgent;

    public String getAuthToken() throws IOException {

        Elements elementsByAttribute = getUrl(url + "/es").
                parse().getElementsByAttribute("data-main");

        final String jsFile = elementsByAttribute.get(0).attr("data-main");

        LOG.info("Fichero JS a parsear: " + jsFile);

        String jsContent = getUrl(url + "/assets/" + jsFile + ".js").body();

        String token = parseAuthToken(jsContent);

        LOG.info("Token: " + token);

        return token;
    }

    private Connection.Response getUrl(String url) throws IOException {
        return Jsoup.connect(url).userAgent(userAgent)
                .method(Connection.Method.GET).followRedirects(false).
                        ignoreContentType(true).timeout(timeout).execute();
    }

    private String parseAuthToken(String body) {
        return StringUtils.substringBetween(body, "api.esios.ree.es\",token:\"", "\",locale:\"es\",env:\"production");
    }
}
