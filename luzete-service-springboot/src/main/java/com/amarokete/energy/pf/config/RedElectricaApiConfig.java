package com.amarokete.energy.pf.config;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Santiago on 19/10/2015.
 */
@Configuration
public class RedElectricaApiConfig {

    @Value("${crawler.proxy.active:false}")
    private boolean proxyActive;

    @Value("${crawler.proxy.host}")
    private String proxyHost;

    @Value("${crawler.proxy.port}")
    private Integer proxyPort;

    @Value("${api.connection.timeout}")
    private Integer connectionTimeout;

    @Value("${api.connection.random.wait}")
    private Integer connectionRandomWait;

    @Bean
    public ClientHttpRequestFactory clientHttpRequestFactory() {

            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(connectionTimeout);

        if (proxyActive) {
            HttpClient defaultHttpClient = (HttpClient) requestFactory
                    .getHttpClient();
            HttpHost proxy = new HttpHost(proxyHost, proxyPort);
            defaultHttpClient.getParams().setParameter(
                    ConnRoutePNames.DEFAULT_PROXY, proxy);
        }

        return requestFactory;
    }

    @Bean
    public RestTemplate restTemplate(
            ClientHttpRequestFactory clientHttpRequestFactory) {

        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);

        final List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        restTemplate.setInterceptors(interceptors);

        return restTemplate;
    }
}
