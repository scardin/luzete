package com.amarokete.energy.pf.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Santiago on 19/10/2015.
 */
@Document(collection = "prices")
public class DailyEnergyPrice implements Serializable {

    @Id
    private String id;

    private Date timestamp;

    private List<Indicator> indicators;

    public DailyEnergyPrice() {

    }

    public DailyEnergyPrice(String id, Date timestamp, List<Indicator> indicators) {
        this.id = id;
        this.timestamp = timestamp;
        this.indicators = indicators;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void addIndicator(Indicator indicator) {
        if (indicators == null) {
            indicators = new ArrayList<>();
        }
        indicators.add(indicator);
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Collection<Indicator> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Indicator> indicators) {
        this.indicators = indicators;
    }

    @Override
    public String toString() {
        return "DailyEnergyPrice{" +
                "id='" + id + '\'' +
                ", timestamp=" + timestamp +
                ", indicators=" + indicators +
                '}';
    }
}
