package com.amarokete.energy.pf.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Santiago on 19/10/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Indicator implements Serializable {

    private String name;

    private Integer id;

    private List<IndicatorValue> values;

    public Indicator() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<IndicatorValue> getValues() {
        return values;
    }

    public void setValues(List<IndicatorValue> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "Indicator{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", values=" + values +
                '}';
    }
}
