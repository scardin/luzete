package com.amarokete.energy.pf.service.repository;

import com.amarokete.energy.pf.domain.DailyEnergyPrice;

import java.util.Date;
import java.util.Optional;

/**
 * Created by Santiago on 31/10/2015.
 */
public interface EnergyPriceRepositoryCustom {

    Optional<DailyEnergyPrice> findIndicatorByDay (Date timestamp, Integer indicator);
}
