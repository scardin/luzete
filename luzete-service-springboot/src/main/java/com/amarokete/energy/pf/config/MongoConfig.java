package com.amarokete.energy.pf.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by Santiago on 19/10/2015.
 */
@Configuration
@EnableAutoConfiguration
@EnableMongoRepositories("com.amarokete.energy.pf.service.repository")
public class MongoConfig {

}
