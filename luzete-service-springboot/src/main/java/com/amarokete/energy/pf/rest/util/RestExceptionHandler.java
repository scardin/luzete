/*
 * Copyright 2013 Produban Servicios Informáticos Generales S.L.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.amarokete.energy.pf.rest.util;

import com.amarokete.energy.pf.service.IndicatorNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Rest exception handler methods.
 */
@ControllerAdvice
public class RestExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestExceptionHandler.class);

    /**
     * Provides 404 - NOT FOUND response in the event of a {@link IndicatorNotFoundException}.
     *
     * @param ex the captured exception
     */
    @ExceptionHandler(IndicatorNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "indicator not found")
    private void handleNotFound(IndicatorNotFoundException ex) {
        LOGGER.debug("Indicator not found");
    }

    /**
     * Provides 400 - BAD REQUEST response in the event of a {@link MethodArgumentNotValidException}.
     *
     * @param ex the captured exception
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "validation failed")
    private void handleValidationError(MethodArgumentNotValidException ex) {
        LOGGER.debug("Validation failed");
    }

    /**
     * Provides 500 - INTERNAL SERVER ERROR response in the event of a {@link RuntimeException}.
     *
     * @param ex the captured exception
     */
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "internal server error")
    private void handleRuntimeError(RuntimeException ex) {
        LOGGER.error("Internal server error", ex);
    }

}
