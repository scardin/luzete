## Uso de Docker

### Construir imagen

     $ mvn clean package docker:build -Dmaven.test.skip

### Push a docker.io

     $ mvn docker:push
     
### Pull desde host

     $ docker pull amarokete/luzete-service-springboot   
    
### Ejecución en host (background)
    
     $ docker run -d --add-host=mongodb:172.17.42.1 -p 8080:8080 amarokete/luzete-service-springboot
     
     $ docker run -d --name eureka -p 8761:8761 springcloud/eureka
     
     $ docker exec -i -t <CONTAINER ID> bash